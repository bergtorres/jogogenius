﻿using System;
using System.Collections.Generic;

namespace JogoGenius
{
    public class ControllerJogador
    {

        public List<string> listaDeJogadasDoUsuario = new List<string>();

        public ControllerJogador()
        {
        }

        public void ZerarJogadasDoUsuario()
        {
            listaDeJogadasDoUsuario.Clear();
            Console.WriteLine("ZEROU A LISTA DO USUÁRIO");
        }

        public void AdicionarJogadaDoUsuario(string jogada)
        {
            listaDeJogadasDoUsuario.Add(jogada);
            Console.WriteLine("ADD JOGADA DO USUÁRIO");
        }
    }
}
