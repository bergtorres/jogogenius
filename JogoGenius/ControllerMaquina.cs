﻿using System;
using System.Collections.Generic;

namespace JogoGenius
{
    public class ControllerMaquina
    {
        public List<string> listaDeJogadasDaMaquina = new List<string>();

        public ControllerMaquina()
        {
        }

        public void ZerarJogadasDaMaquina()
        {
            listaDeJogadasDaMaquina.Clear();
            Console.WriteLine("ZEROU A LISTA DA MÁQUINA");
        }

        public void AdicionarJogadaDaMaquina(string jogada)
        {
            listaDeJogadasDaMaquina.Add(jogada);
            Console.WriteLine("ADD JOGADA DA MÁQUINA");
        }
    }
}
