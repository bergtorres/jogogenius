﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace JogoGenius
{
    public partial class MainPage : ContentPage
    {
        string[] arrayDeCores = { "AZUL", "AMARELO", "VERDE", "VERMELHO" };
        int[] arrayDeJogadasPorNivel = { 8, 14, 20, 31 };

        public Color Verde { get; } = Color.FromHex("#006400");
        public Color Amarelo { get; } = Color.FromHex("#eded00");
        public Color Vermelho { get; } = Color.FromHex("#e20000");
        public Color Azul { get; } = Color.FromHex("#00008B");

        public Color Verde_highlight { get; } = Color.FromHex("#b2ffc7");
        public Color Amarelo_highlight { get; } = Color.FromHex("#f8ffb2");
        public Color Vermelho_highlight { get; } = Color.FromHex("#ffb2b2");
        public Color Azul_highlight { get; } = Color.FromHex("#b2b3ff");

        public int Nivel { get; set; } = 1;

        ControllerJogador Jogador;
        ControllerMaquina Maquina;
        Random random;

        public MainPage()
        {
            InitializeComponent();

            BindingContext = this;

            ConfigurarClickDosBotoes();

            Jogador = new ControllerJogador();
            Maquina = new ControllerMaquina();
            random = new Random();
        }

        // INICIAR JOGO

        async void IniciarJogo()
        {
            Console.WriteLine("INICIANDO JOGO");

            btnIniciar.Text = "REINICIAR";
            labelNivel.IsVisible = true;
            labelNivel.Text = "";

            Jogador.ZerarJogadasDoUsuario();
            Maquina.ZerarJogadasDaMaquina();

            await Task.Delay(1000);
            HabilitarBotoesDeJogar();
            DesabilitarBotoesDeNivel();
            AdicionarCorAleatoria();
        }

        void AdicionarCorAleatoria()
        {
            int indice = random.Next(0, arrayDeCores.Length);
            var cor = arrayDeCores[indice];

            if (Maquina.listaDeJogadasDaMaquina.Count.Equals(arrayDeJogadasPorNivel[Nivel - 1]))
            {
                ExibirAlerta("PARABÉNS", "VOCÊ VENCEU!!!! MUDE O NÍVEL OU TOQUE EM REINICIAR PARA JOGAR NOVAMENTE!");
                DesabilitaBotoesDeJogar();
                HabilitarBotoesDeNivel();
            }
            else
            {
                Maquina.AdicionarJogadaDaMaquina(cor);
                labelNivel.Text = "Repetições: " + Maquina.listaDeJogadasDaMaquina.Count;
                HighLightsDeJogadasDaMaquina();
            }
        }

        void VerificarJogadasDoUsuario()
        {
            for (int i = 0; i < Jogador.listaDeJogadasDoUsuario.Count; i++)
            {
                if (Jogador.listaDeJogadasDoUsuario[i] != Maquina.listaDeJogadasDaMaquina[i])
                {
                    Console.WriteLine("ERROU " + "MÁQUINA: " + Maquina.listaDeJogadasDaMaquina[i] + " USUÁRIO: " + Jogador.listaDeJogadasDoUsuario[i]);
                    DesabilitaBotoesDeJogar();
                    HabilitarBotoesDeNivel();
                    ExibirAlerta("FIM DE JOGO", "VOCÊ PERDEU!!! MUDE O NÍVEL OU TOQUE EM REINICIAR PARA JOGAR NOVAMENTE!");
                    return;
                }
            }
            FimDaJogada();
        }

        async void HighLightsDeJogadasDaMaquina()
        {
            DesabilitaBotoesDeJogar(); // Para o usuário não ficar jogando junto com a máquina
            foreach (string jogada in Maquina.listaDeJogadasDaMaquina)
            {
                switch (jogada)
                {
                    case "AZUL":
                        await Task.Delay(1000); // Entre um highlight e outro
                        btnAzul.BackgroundColor = Azul_highlight;
                        await Task.Delay(1000);
                        btnAzul.BackgroundColor = Azul;
                        Console.WriteLine("JOGADA DA MÁQUINA AZUL");
                        break;
                    case "AMARELO":
                        await Task.Delay(1000); // Entre um highlight e outro
                        btnAmarelo.BackgroundColor = Amarelo_highlight;
                        await Task.Delay(1000);
                        btnAmarelo.BackgroundColor = Amarelo;
                        Console.WriteLine("JOGADA DA MÁQUINA AMARELO");
                        break;
                    case "VERDE":
                        await Task.Delay(1000); // Entre um highlight e outro
                        btnVerde.BackgroundColor = Verde_highlight;
                        await Task.Delay(1000);
                        btnVerde.BackgroundColor = Verde;
                        Console.WriteLine("JOGADA DA MÁQUINA VERDE");
                        break;
                    case "VERMELHO":
                        await Task.Delay(1000); // Entre um highlight e outro
                        btnVermelho.BackgroundColor = Vermelho_highlight;
                        await Task.Delay(1000);
                        btnVermelho.BackgroundColor = Vermelho;
                        Console.WriteLine("JOGADA DA MÁQUINA VERMELHO");
                        break;
                    default:
                        break;
                }
            }
            HabilitarBotoesDeJogar(); // Após as exibição das jogadas da máquina os botões ficam diposníveis para o  usuário
        }

        // FIM DA JOGDA

        async void FimDaJogada()
        {
            if (Maquina.listaDeJogadasDaMaquina.Count.Equals(Jogador.listaDeJogadasDoUsuario.Count))
            {
                Jogador.ZerarJogadasDoUsuario();
                await Task.Delay(1000);
                AdicionarCorAleatoria();
            }
        }

        // DESABILITAR E HABILITAR 

        void DesabilitaBotoesDeJogar()
        {
            btnAzul.IsEnabled = false;
            btnAmarelo.IsEnabled = false;
            btnVerde.IsEnabled = false;
            btnVermelho.IsEnabled = false;
        }

        void HabilitarBotoesDeJogar()
        {
            btnAzul.IsEnabled = true;
            btnAmarelo.IsEnabled = true;
            btnVerde.IsEnabled = true;
            btnVermelho.IsEnabled = true;
        }

        void DesabilitarBotoesDeNivel()
        {
            btnMais.IsEnabled = false;
            btnMenos.IsEnabled = false;
        }

        void HabilitarBotoesDeNivel()
        {
            btnMais.IsEnabled = true;
            btnMenos.IsEnabled = true;
        }

        // CLICK DOS BOTṌES

        void ConfigurarClickDosBotoes()
        {
            btnIniciar.Clicked += (sender, e) =>
            {
                IniciarJogo();
            };

            btnAzul.Clicked += async (sender, e) =>
            {
                btnAzul.BackgroundColor = Azul_highlight;
                await Task.Delay(500);
                btnAzul.BackgroundColor = Azul;
                Jogador.AdicionarJogadaDoUsuario("AZUL");
                VerificarJogadasDoUsuario();
            };

            btnAmarelo.Clicked += async (sender, e) =>
            {
                btnAmarelo.BackgroundColor = Amarelo_highlight;
                await Task.Delay(500);
                btnAmarelo.BackgroundColor = Amarelo;
                Jogador.AdicionarJogadaDoUsuario("AMARELO");
                VerificarJogadasDoUsuario();
            };

            btnVerde.Clicked += async (sender, e) =>
            {
                btnVerde.BackgroundColor = Verde_highlight;
                await Task.Delay(500);
                btnVerde.BackgroundColor = Verde;
                Jogador.AdicionarJogadaDoUsuario("VERDE");
                VerificarJogadasDoUsuario();
            };

            btnVermelho.Clicked += async (sender, e) =>
            {
                btnVermelho.BackgroundColor = Vermelho_highlight;
                await Task.Delay(500);
                btnVermelho.BackgroundColor = Vermelho;
                Jogador.AdicionarJogadaDoUsuario("VERMELHO");
                VerificarJogadasDoUsuario();
            };

            btnMais.Clicked += (sender, e) =>
            {
                if (Nivel < 4)
                {
                    Nivel += 1;
                    labelSelecionarNivel.Text = "Nível: " + Nivel.ToString();
                }

            };

            btnMenos.Clicked += (sender, e) =>
            {
                if (Nivel > 1)
                {
                    Nivel -= 1;
                    labelSelecionarNivel.Text = "Nível: " + Nivel.ToString();
                }
            };
        }

        // ALERTA

        void ExibirAlerta(string titulo, string mensagem)
        {
            DisplayAlert(titulo, mensagem, "Ok");
        }
    }
}
